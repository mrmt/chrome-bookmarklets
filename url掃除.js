javascript:(function(L,H,P){
    if(H == 'www.amazon.co.jp'){
	if(P.match(/\/(gp\/product|dp)\/([0-9A-Z]+)/)){
	    L.href = '/dp/' + RegExp.$2 + '/';
	}
    }else{
	var k = [], c = [], kv;
	var argv = L.search.replace(/^\?/,'').split('&');
	var argc = argv.length;
	while(argc--){
	    if(!argv[argc].match(/^(utm_|fb_ref|fb_action_|fb_source|action_object_map|action_type_map|action_ref_map|__from)/)){
		c.push(argv[argc]);
	   }
	    kv = argv[argc].split('=');
	    k[kv[0]]=kv[1];
	}
	if(H == 'mportal.cybozu.com'){
	    if(P == '/g/schedule/view.csp'){
		L.href = P + '?' + 'event=' + k['event'];
	    }
	}else{
	    L.href = P + ((c.length>1) ? '?'+c.join('&') : '');
	}
    }
})(location, location.hostname, location.pathname);
